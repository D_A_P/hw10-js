1. document.createElement() - дозволяє створити новий DOM-елемент;
   document.createTextNode() - дозволяє створити новий текстовий вузол з вказаним текстом;
   element.insertAdjacentElement() - дозволяє вставляти DOM-елемент в певному місці вказаного елемента;
   element.insertAdjacentHTML() - ддозволяє вставляти HTML-код;
   element.appendChild() - дозволяє додати дочірній елемент до батьківського елемента;
   element.innerHTML() - дозволяє додавати HTML елементи безпосередньо в DOM.

2. -знайти елемент з класом "navigation"
   const еlements = document.querySelectorAll(".navigation");
   -знайти елемент, який потрібно видалити (наприклад, перший елемент у колекції)
   const elementToRemove = elements[0];
   -видалити елемент зі сторінки
   elementToRemove.remove();

3. Методи для вставки DOM-елементів:
   -node.append(...nodes або strings) – додає вузли або рядки в кінець вузла;
   -node.prepend(...nodes або strings) – вставляє вузли або рядки в початок вузла;
   -node.before(...nodes або strings) – вставляє вузли або рядки перед вузлом;
   -node.after(...nodes або strings) – вставляє вузли або рядки після вузла.
