'use strict';

const newElement = document.createElement("a");
newElement.textContent = 'Learn More';
newElement.setAttribute("href", "#");
const footer = document.querySelector("footer");
const paragraph = footer.querySelector("p");
paragraph.after(newElement);



const selectElement = document.createElement("select");
selectElement.id = "rating";
const main = document.querySelector("main");
const sectionFeatures = main.querySelector(".features");
sectionFeatures.before(selectElement);

const optionElements = [
    {   value: 4,
        text: "4 Stars",
    },
    {   value: 3,
        text: "3 Stars",
    },
    {   value: 2,
        text: "2 Stars",
    },
    {   value: 1,
        text: "1 Stars",
    },
];

for (let i = 0; i < optionElements.length; i++ ) {
    const option = document.createElement("option");
    option.value = optionElements[i].value;
    option.text = optionElements[i].text;
    selectElement.append(option);
}

